# LABORATORIO 21 NOVEMBRE 2017

## Obiettivi

- Esplorare gli internals di Git
- Familiarizzare con Git, Git-flow, hosting remoto

## Installazioni

- (se necessario) installare git-flow
    ```bash
    curl -OL https://raw.github.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh
    chmod +x gitflow-installer.sh
    rm -rf gitflow
    PREFIX=~ ./gitflow-installer.sh install stable
    echo 'PATH=${PATH}:~/bin' >> ~/.bashrc
    ```
    
- (se necessario) registrarsi sul sito https://gitlab.com  
  _è possibile usare anche una sdelle seguenti login_:
  ![image](/uploads/6620f21374789a961b182afc237596b4/image.png)


## Lavoro

1. fare `fork` del repository https://gitlab.com/svigruppo/git-internals
2. fare clone in locale  
   Troverete un ramo `develop` e un ramo `master` oltre ad alcuni commmit taggati con vari nomi
3. date il comando `git flow init`
4. se volete usare eclipse date il comando `./gradlew eclipse` e poi potete importare il progetto da Eclipse come _existing project_

Procedere iterativamente a questo punto 
- guardando il test (scritto in JUnit) presente nella directory `src/test/java/core`  
  _potete eseguirlo sia da eclipse che con il comando_ `./gradlew test`
- scrivendo il codice necessario per farlo passare (le classi vanno messe in `src/main/java/core`)
- ripulendo il codice (refactoring)
- incorporando il successivo test (vedi sotto)

Al termine ovunque siate arrivati, prevedete di consegnare il compito creando una release taggata come "fineLaboratorio" e eseguire il push del ramo master.

Chiaramente siete invitati nel caso non abbiate completato il lavoro a farlo a casa e a fare seconda release taggata come "fineLavoro"



### NOTE

- Ogni test dovrà corrispondere alla creazione di una nuova feature git-flow (usate quindi i comandi di partenza e conclusione opportuni)
- Per incorporare il test successivo da implementare, usare il comando `git cherry-pick <tagname>`
- Dove nei test trovate `???` dovete seguire le seguire le indicazioni dei commenti-TODO e sostuirli con i valori trovati tramite operazioni nella shell

### SUGGERIMENTI
- per la funzione di inflate si può comodamente usare la classe `InflaterInputStream` o funzione `inflate` dalla libreria standard `java.util.zip`
- per documentazione riguardo ai formati internals di Git potete riferirvi a:
    - https://git-scm.com/book/en/v2/Git-Internals-Git-Objects
    - http://stackoverflow.com/a/21599232
- fate attenzione per le parti binarie del file (ad es. oggetto tree) a come trattate i dati (se come byte o stringhe con uno specifico encoding)

