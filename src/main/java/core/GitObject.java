package core;

public abstract class GitObject {
	
	public abstract String getType();
	public abstract String getHash();
}
