package core;

import java.io.FileInputStream;
import java.util.zip.InflaterInputStream;

public class GitCommitObject extends GitObject {
	
	private String path;
	private String commitHash;

	public GitCommitObject(String repoPath, String masterCommitHash) {
		path = repoPath;
		commitHash = masterCommitHash;
	}

	public String getHash() {
		return commitHash;
	}
	
	public String getCommit() {
		String value = "";
		try {
			InflaterInputStream kappa =new InflaterInputStream (new FileInputStream(path + "/objects/"+ commitHash.substring(0,2) + "/" + commitHash.substring(2)));
			byte []b= new byte[1024];
			kappa.read(b);
			StringBuilder sbuilder = new StringBuilder();
			boolean check = false;
			for(int i = 0; i < b.length; i++) {
				if((char)b[i] == '\0' && !check){
					sbuilder.append('|');
					check = true;
				} else{		
				sbuilder.append((char)b[i]);
				}
			}
			sbuilder.append('\0');
			value = sbuilder.toString();
			kappa.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public String getTreeHash() {
		String value = getCommit();
		return value.substring(value.indexOf("tree") + 5, value.indexOf('\n'));
	}

	public String getParentHash() {
		String value = getCommit();
		return value.substring(value.indexOf("parent") + 7, value.indexOf('\n', value.indexOf("parent")));
	}

	public String getAuthor() {
		String value = getCommit();
		return value.substring(value.indexOf("author") + 7, value.indexOf('>') + 1);
	}

	@Override
	public String getType() {
		return "commit";
	}

}
