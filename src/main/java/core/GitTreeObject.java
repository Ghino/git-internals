package core;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.InflaterInputStream;

public class GitTreeObject extends GitObject{
	private String path;
	private String hash;
	private HashMap<String,String> myPaths = new HashMap<String,String>();
	public GitTreeObject(String repo, String name) {
		this.path = repo;
		this.hash = name;
	}

	public  Set<String> getEntryPaths() {
		String toWork= objectReader();
		toWork=toWork.substring(toWork.indexOf(" "));
		toWork=toWork.substring(toWork.indexOf("|")+1, toWork.indexOf("|00"));
		String [] myWork=toWork.split("\n");
		for(String i:myWork) {
			String []temp=i.split("[| ]");
			myPaths.put(temp[1], temp[2]);
		}
		return myPaths.keySet();
	}

	public GitObject getEntry(String string) {
		String path=myPaths.get(string);
		if (string.indexOf("dir")==-1) {
			return new GitBlobObject(this.path, path);
		}
		return new GitTreeObject(this.path,hash);
	}
	
	public String objectReader() {
		try {
			byte[] myBuffer= new byte[1024];
			InflaterInputStream inf= new InflaterInputStream(new FileInputStream (path + "/objects/"+getHash().substring(0, 2)+ "/"+getHash().substring(2)));
			inf.read(myBuffer);
			StringBuilder sb= new StringBuilder();
			int start=0;
			int check20=0;
			for (int i =0;i < myBuffer.length;i++) {	
				if ((char)myBuffer[i]!='\0' && start<2) {
					sb.append((char)myBuffer[i]);
				} else {
					if (start<2){
						start++;
						sb.append("|");
					}else if (start>=2){
						check20++;
						sb.append(String.format("%02x", myBuffer[i]));
						if (check20%20==0) {
							sb.append("\n");
							start=1;
						}
					} else {
						sb.append("|");
					}
				}
			}
			String mys= sb.toString();
			inf.close();
			return mys;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "error";
	}

	@Override
	public String getType() {
		return "tree";
	}

	@Override
	public String getHash() {
		return this.hash;
	}
}
