package core;

import java.io.BufferedReader;
import java.io.FileReader;

public class GitRepository {
	
	private String path;

	public GitRepository(String repoPath) {
		path = repoPath;
	}

	public String getHeadRef() {
		String value = "";
		try {
			BufferedReader kappa = new BufferedReader(new FileReader(path + "/HEAD"));
			value = kappa.readLine();
			value = value.substring(5);
			kappa.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return value;
	}

	public String getRefHash(String string) {
		String value = "";
		
		try {
			BufferedReader kappa = new BufferedReader(new FileReader(path + "/" + string));
			value = kappa.readLine();
			kappa.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return value;
	}

}
