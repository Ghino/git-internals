package core;

import java.io.FileInputStream;
import java.util.zip.InflaterInputStream;

public class GitBlobObject extends GitObject {
	
	private String path;
	private String hash;

	public GitBlobObject(String repoPath, String string) {
		path = repoPath;
		hash = string;
	}

	public String getBlob() {
		String value = "";
		try {
			InflaterInputStream kappa =new InflaterInputStream (new FileInputStream(path + "/objects/"+ hash.substring(0,2) + "/" + hash.substring(2)));
			byte []b= new byte[1024];
			kappa.read(b);
			StringBuilder sbuilder = new StringBuilder();
			boolean check = false;
			for(int i = 0; i < b.length; i++) {
				if((char)b[i] == '\0' && !check){
					sbuilder.append('|');
					check = true;
				} else{		
				sbuilder.append((char)b[i]);
				}
			}
			sbuilder.append('\0');
			value = sbuilder.toString();
			kappa.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return value;
	}

	public String getContent() {
		String s = getBlob();
		return s.substring(s.indexOf("|") + 1, s.indexOf('\0'));
		
	}
	public String getType() {
		return getBlob().substring(0,4);
	}

	@Override
	public String getHash() {
		return hash;
	}


}
